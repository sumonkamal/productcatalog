export class Product {
    id: number;
    code: string;
    name: string;
    price: number;
    lastUpdated: string;
    photo: string;

    constructor(id: number,code: string,name: string,price: number,lastUpdated: string,photo: string) {
       
        this.id= id;
        this.code= code;
        this.name = name;
        this.price = price;
        this.lastUpdated = lastUpdated;
        this.photo = photo;
    }
}

export class ProductViewModel{
    code: string;
    name: string;
    price: number;
    picture: File;
    id: number;
    constructor(code: string,name: string,price: number,picture: File,id:number=0) {
        this.code= code;
        this.name = name;
        this.price = price;
        this.picture = picture;
        this.id = id;
    }
}
