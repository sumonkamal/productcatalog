import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from '@angular/router';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private router: Router, private productService: ProductService) { }

  addForm: FormGroup;
  productImagePath: string;
  hasError: boolean;
  errorMessage:string;
  ngOnInit() {

    this.addForm = this.formBuilder.group({
      name: ['', Validators.required],
      code: ['', Validators.required],
      price: ['', Validators.required]
    });

  }

  fileToUpload: File = null;


  onSubmit() {
    let price = <Number>this.addForm.controls["price"].value;
    if(price > 999){
     if(confirm("Are you sure you want to add product with price Greater than 999?")) {
       this.addProduct();
       }
    }
    else{
      this.addProduct();
    }
  }

  addProduct(){
    this.productService.createProduct(this.addForm.value,this.fileToUpload)
    .subscribe( data => {
      this.router.navigate(['list']);
    },
      error => {
        console.log("Error: "+JSON.stringify(error.error.errors));
        this.hasError = true;
        this.errorMessage = JSON.stringify(error.error.errors);
      });
  }


  handleFileInput(event){
    if (event.target.files && event.target.files[0]) {
      this.fileToUpload=event.target.files[0];
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => { 
        this.productImagePath = reader.result.toString();
      }
    }
  }

  
}
