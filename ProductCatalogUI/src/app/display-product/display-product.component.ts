import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product.service';
import { Product } from '../model/product';

import {Router} from '@angular/router';
import { ExcelService } from '../services/excel.service';

@Component({
  selector: 'app-display-product',
  templateUrl: './display-product.component.html',
  styleUrls: ['./display-product.component.css']
})
export class DisplayProductComponent implements OnInit {

  products: Product[];
  nameSearchValue: string="";
  codeSearchValue: string="";
  priceSearchValue: number=0;

  constructor(private router: Router,private productService: ProductService,private excelService: ExcelService) {
        
   }

  ngOnInit() {
    this.productService.getProducts()
    .subscribe( data => {
      this.products = data;
    });
  }

  deleteProduct(product: Product): void {
    this.productService.deleteProduct(product.id)
      .subscribe( data => {
        this.products = this.products.filter(u => u !== product);
      })
  };

  editProduct(product: Product): void {
    localStorage.removeItem("editProductId");
    localStorage.setItem("editProductId", product.id.toString());
    this.router.navigate(['update']);
  };

  addProduct(): void {
    this.router.navigate(['create']);
  };

  onSearchChange(){
    this.productService.getProducts(this.nameSearchValue,this.codeSearchValue,this.priceSearchValue)
    .subscribe( data => {
      this.products = data;
    });
  }

  exportAsXLSX():void {
    this.excelService.exportAsExcelFile(this.products, 'Products');
 }

}
