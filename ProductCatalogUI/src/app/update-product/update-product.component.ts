import { Component, OnInit } from '@angular/core';
import { Product } from '../model/product';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { ProductService } from '../services/product.service';
import {Router} from '@angular/router';
import {first} from "rxjs/operators";
import { HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {

  product: Product;
  editForm: FormGroup;
  hasError: boolean = false;
  errorMessage: string= "";
  productImagePath: string;
  fileToUpload: File;


  constructor(private formBuilder: FormBuilder,private router: Router, private productService: ProductService) { }

  ngOnInit() {
    let productId = localStorage.getItem("editProductId");
    if(!productId) {
      alert("Invalid action.")
      this.router.navigate(['list']);
      return;
    }
    this.editForm =this.formBuilder.group({
      id: [],
      name: ['', Validators.required],
      code: ['', Validators.required],
      price: ['', Validators.required]
    });
   
    this.productService.getProductById(Number(productId))
      .subscribe( data => {
        this.productImagePath = data.photo;
        this.editForm.setValue({
          id: data.id, 
          price: data.price,
          code: data.code,
          name: data.name
        });
      });
  }

  onSubmit() {
    let price = <Number>this.editForm.controls["price"].value;
    if(price > 999){
     if(confirm("Are you sure you want to add product with price Greater than 999?")) {
       this.updateProduct();
       }
    }
    else{
      this.updateProduct();
    }
   
  }

  updateProduct(){
    this.productService.updateProduct(this.editForm.value,this.fileToUpload)
    .pipe(first())
    .subscribe(
      data => {
        this.router.navigate(['list']);
      },
      error => {
        console.log("Error: "+JSON.stringify(error.error.errors));
        this.hasError = true;
        this.errorMessage = JSON.stringify(error.error.errors);
      });
  }

  handleFileInput(event){
    if (event.target.files && event.target.files[0]) {
      this.fileToUpload=event.target.files[0];
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => { 
        this.productImagePath = reader.result.toString();
      }
    }
   
  }

  

}
