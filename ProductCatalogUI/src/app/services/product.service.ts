import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {of, Observable} from 'rxjs/'
import { Product, ProductViewModel } from '../model/product';
import { environment } from 'src/environments/environment';
import { solutionConstants } from '../SolutionConstants';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  productUrl: string = environment.apiBaseUrl + solutionConstants.API_URL;
  constructor(private http: HttpClient) {
   
   }

  getProducts(name: string="",code: string="",price:number=0): Observable<Product[]>{
   return this.http.get<Product[]>(this.productUrl+"/?name="+name +"&code="+code+"&price="+price);
  }

  getProductById(id: number) {
    return this.http.get<Product>(this.productUrl + '/' + id);
  }

  createProduct(productViewModel: ProductViewModel,productPictureFile: File = null) {
    let formData= this.setFormData(productViewModel,productPictureFile);
    return this.http.post(this.productUrl,formData);
  }

  deleteProduct(id: number) {
    return this.http.delete(this.productUrl + '/' + id);
  }

  updateProduct(productViewModel: ProductViewModel,productPictureFile: File = null) {
    let formData= this.setFormData(productViewModel,productPictureFile);
    return this.http.put(this.productUrl + '/' + productViewModel.id, formData);
  }

  setFormData(productViewModel: ProductViewModel,productPictureFile: File = null): FormData{
    const formData = new FormData();
    formData.append("code",productViewModel.code);
    formData.append("name",productViewModel.name);
    formData.append("price",productViewModel.price.toString());
    formData.append("picture",productPictureFile);
    return formData;

  }

}
