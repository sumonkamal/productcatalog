﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace ProductCatalog.Repository
{
    public class MsSqlDatabase<TContext> : ConnectionStringOrientedDbProvider<TContext>, IDatabase<TContext> where TContext : DbContext
    {
        public MsSqlDatabase(IConfiguration configuration) : base(configuration)
        {
        }

        public DbContextOptionsBuilder<TContext> CreateDbContextOptionBuilder()
        {
            var connectionString = GetConnectionString();
            var contextOptionsBuilder = new DbContextOptionsBuilder<TContext>();
            return contextOptionsBuilder.UseSqlServer(connectionString, sqlOptions =>
            {
                sqlOptions.MigrationsAssembly(typeof(TContext).Assembly.GetName().Name);
            });

        }

    }
}