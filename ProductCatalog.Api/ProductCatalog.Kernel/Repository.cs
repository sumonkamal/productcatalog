using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProductCatalog.Repository.Models;

namespace ProductCatalog.Repository
{
    public class Repository<TContext> : ReadOnlyRepository<TContext>, IRepository
    where TContext : DbContext
    {
        public Repository(IContextFactory<TContext> contextFactory)
            : base(contextFactory,false)
        {
        }

        public virtual void Create<TEntity>(TEntity entity, string createdBy = null)
            where TEntity : class, IEntity
        {
            entity.LastUpdated = DateTime.Now;
            Context.Set<TEntity>().Add(entity);
        }

        public virtual void Update<TEntity>(TEntity entity, string modifiedBy = null)
            where TEntity : class, IEntity
        {
            entity.LastUpdated = DateTime.Now;
            Context.Set<TEntity>().Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete<TEntity>(object id)
            where TEntity : class, IEntity
        {
            TEntity entity = Context.Set<TEntity>().Find(id);
            Delete(entity);
        }

        public virtual void Delete<TEntity>(TEntity entity)
            where TEntity : class, IEntity
        {
            var dbSet = Context.Set<TEntity>();
            if (Context.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dbSet.Remove(entity);
        }

        public virtual void Save()
        {
            try
            {
                Context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                HandleConcurrencyConflicts(ex);
            }
        }

        public async Task SaveAsync()
        {
            try
            {
                await Context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
               HandleConcurrencyConflicts(ex);
            }
        }

        public void HandleConcurrencyConflicts(DbUpdateConcurrencyException ex)
        {
            // Database wins approach during concurrency conflicts
            var data = ex.Entries.Single();
            data.OriginalValues.SetValues(data.GetDatabaseValues());

        }
     
    }
}