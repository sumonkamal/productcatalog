﻿using System.ComponentModel.DataAnnotations;

namespace ProductCatalog.Repository.Models
{
    public class ConcurrencyCheckEnabledEntity
    {
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}