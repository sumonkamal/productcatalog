﻿using System;

namespace ProductCatalog.Repository.Models
{
    public interface IEntity
    {
        int Id { get; set; }
        DateTime LastUpdated { get; set; }
    }
}