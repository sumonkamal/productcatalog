﻿using Microsoft.EntityFrameworkCore;
using ProductCatalog.Repository.EFConfigurations;
using ProductCatalog.Repository.Models;

namespace ProductCatalog.Repository
{
    public class ProductCatalogContext : DbContext
    {
        public ProductCatalogContext(DbContextOptions options): base(options)
        {
        }

        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
            base.OnModelCreating(modelBuilder);

        }
    }
}