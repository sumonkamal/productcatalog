﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace ProductCatalog.Repository
{
    public class ProductCatalogContextFactory : IDesignTimeDbContextFactory<ProductCatalogContext>
    {
       
        public ProductCatalogContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ProductCatalogContext>();
            builder.UseSqlServer("ProductCatalog");
            return new ProductCatalogContext(builder.Options);
        }
    }
}