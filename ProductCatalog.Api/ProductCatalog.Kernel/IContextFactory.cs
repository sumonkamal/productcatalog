﻿using Microsoft.EntityFrameworkCore;

namespace ProductCatalog.Repository
{
    public interface IContextFactory<T> where T : DbContext
    {
        T Create();
        DbContextOptionsBuilder<T> CreateDatabaseOptionBuilder();
    }
}