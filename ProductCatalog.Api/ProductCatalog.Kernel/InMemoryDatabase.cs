﻿using Microsoft.EntityFrameworkCore;

namespace ProductCatalog.Repository
{
    public class InMemoryDatabase<TContext> : IDatabase<TContext>
        where TContext : DbContext
    {
        public DbContextOptionsBuilder<TContext> CreateDbContextOptionBuilder()
        {
            var contextOptionsBuilder = new DbContextOptionsBuilder<TContext>();
            return contextOptionsBuilder.UseInMemoryDatabase(nameof(TContext));
        }
    }
}