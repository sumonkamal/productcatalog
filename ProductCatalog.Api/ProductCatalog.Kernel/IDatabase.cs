﻿using Microsoft.EntityFrameworkCore;

namespace ProductCatalog.Repository
{
    public interface IDatabase<TContext> where TContext : DbContext
    {
        DbContextOptionsBuilder<TContext> CreateDbContextOptionBuilder();
    }
}