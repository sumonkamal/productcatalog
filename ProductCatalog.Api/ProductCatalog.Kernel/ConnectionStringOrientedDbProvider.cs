﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace ProductCatalog.Repository
{
    public abstract class ConnectionStringOrientedDbProvider<TContext> where TContext : DbContext
    {
 
        private readonly IConfiguration _configuration;
        protected ConnectionStringOrientedDbProvider(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string GetConnectionString()
        {
            var contextType = typeof(TContext);
            var connectionString = _configuration.GetConnectionString(contextType.Name);
            ValidateDefaultConnectionString(connectionString);
            return connectionString;
        }

        private void ValidateDefaultConnectionString(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentNullException(nameof(connectionString), $"Connection string is Null or empty for context : {typeof(TContext)}");
            }
        }
    }
}