﻿using System;
using Microsoft.EntityFrameworkCore;

namespace ProductCatalog.Repository
{
    public class ContextFactory<TContext> : IContextFactory<TContext> where TContext : DbContext
    {
        private readonly IDatabase<TContext> _database;

        public ContextFactory(
            IDatabase<TContext> database)
        {
            _database = database;
        }

        public TContext Create()
        {
            return Activator.CreateInstance(typeof(TContext), CreateDatabaseOptionBuilder().Options) as TContext;
        }

        public DbContextOptionsBuilder<TContext> CreateDatabaseOptionBuilder()
        {
            return _database.CreateDbContextOptionBuilder();
        }

    }
}
