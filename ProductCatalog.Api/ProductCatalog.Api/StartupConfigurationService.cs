﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ProductCatalog.Api.Extensions;
using ProductCatalog.Repository;

namespace ProductCatalog.Api
{
    public class StartupConfigurationService : IStartupConfigurationService
    {
        

        public virtual void ConfigureEnvironment(IHostingEnvironment env)
        {
        }

        public virtual void ConfigureService(IServiceCollection services, IConfigurationRoot configuration)
        {
           services.RegisterTypes();
        }

        public virtual void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var serviceProvider = serviceScope.ServiceProvider;
                var contextFactory= serviceProvider.GetRequiredService<IContextFactory<ProductCatalogContext>>();
                contextFactory.Create()?.Database.Migrate();
            }
        }

    }
}