﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using ProductCatalog.Api.ViewModel;
using ProductCatalog.Domain.Commands;
using ProductCatalog.Domain.Queries;

namespace ProductCatalog.Api.Controllers
{
    /// <summary>
    /// Products related operations
    /// </summary>
    [ApiVersion("1.0")]
    [ApiVersion("2.0")]
    [Route("api/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IMediator _mediator;

        /// <inheritdoc />
        public ProductsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Gets List of Products based on the parameters
        /// </summary>
        /// <param name="name">The name of the product</param>
        /// <param name="code">The Product Code</param>
        /// <param name="price">The price of the product</param>
        /// <returns></returns>

        [HttpGet, MapToApiVersion("1.0")]
        public async Task<IActionResult> Get([FromQuery]string name, [FromQuery]string code, [FromQuery]decimal price)
        {
            var result = await _mediator.Send(new SearchProductQuery(code, price, name));
            return Ok(result);
        }

        /// <summary>
        /// Gets list of products based on name only
        /// </summary>
        /// <param name="name">The product name</param>
        /// <returns></returns>
        [HttpGet, MapToApiVersion("2.0")]
        public async Task<IActionResult> GetV2([FromQuery]string name)
        {
            var result = await _mediator.Send(new SearchProductQuery("", 0, name));
            return Ok(result);
        }

        /// <summary>
        /// Gets a Product by specified Id 
        /// </summary>
        /// <param name="id">The product Id</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _mediator.Send(new GetSingleProductQuery(id));
            if (result == null)
            {
                return NotFound($"Product with specified Id: {id} does not exist");
            }
            return Ok(result);
        }

        /// <summary>
        /// Creates a new Product
        /// </summary>
        /// <param name="viewModel">The product creation ViewModel</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromForm]ProductViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var command = new CreateProductCommand(viewModel.Code, viewModel.Name, viewModel.Price, viewModel.Picture);
            var result = await _mediator.Send(command);
            if (result.HasError)
            {
                return BadRequest(result);
            }
            return Ok(result);
        }

        /// <summary>
        /// Updates a product specified by the ViewModel data
        /// </summary>
        /// <param name="id">The Id of the product to be updated</param>
        /// <param name="viewModel">The Product ViewModel</param>
        /// <returns></returns>

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromForm]ProductViewModel viewModel)
        {
            var command =
                new UpdateProductCommand(id, viewModel.Code, viewModel.Name, viewModel.Picture, viewModel.Price)
                {
                    ProductId = id
                };
            var result = await _mediator.Send(command);
            if (result.HasError)
            {
                return BadRequest(result);
            }
            return Ok(result);
        }

        /// <summary>
        /// Deletes a product specified by the Id
        /// </summary>
        /// <param name="id">The product Id</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _mediator.Send(new DeleteProductCommand(id));
            if (!result)
            {
                return NotFound("Product to delete does not exist");
            }
            return Ok();
        }


    }
}
