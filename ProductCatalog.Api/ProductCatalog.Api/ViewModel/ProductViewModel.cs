﻿using Microsoft.AspNetCore.Http;

namespace ProductCatalog.Api.ViewModel
{
    public class ProductViewModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public decimal Price { get; set; }

        public IFormFile Picture { get; set; }
    }
}