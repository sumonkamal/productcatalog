﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.DependencyInjection;
using ProductCatalog.Domain.Commands;
using ProductCatalog.Repository;
using Swashbuckle.AspNetCore.Swagger;

namespace ProductCatalog.Api.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterTypes(this IServiceCollection services)
        {
            services.AddScoped<IContextFactory<ProductCatalogContext>, ContextFactory<ProductCatalogContext>>();
            services.AddScoped<IDatabase<ProductCatalogContext>, MsSqlDatabase<ProductCatalogContext>>();
            services.AddScoped<IRepository, Repository<ProductCatalogContext>>();
            services.AddScoped<IReadOnlyRepository, ReadOnlyRepository<ProductCatalogContext>>();
            services.AddSingleton<IFileService, FileService>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        public static void AddApiVersioningWithSwagger(this IServiceCollection services)
        {
            services.AddVersionedApiExplorer();
            services.AddApiVersioning(o =>
            {
                // o.ReportApiVersions = true;
                o.ApiVersionReader = new HeaderApiVersionReader("api-version");
                o.DefaultApiVersion = ApiVersion.Default;
                o.AssumeDefaultVersionWhenUnspecified = true;
            });
            services.AddSwaggerGen(
                options =>
                {
                    //  options.SwaggerDoc("v1", new Info { Title = "ProductCatalog API", Version = "v1" });
                    var provider = services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();
                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerDoc(description.GroupName, CreateInfoForApiVersion(description));
                    }

                    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.XML";
                    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                    options.IncludeXmlComments(xmlPath);
                });
        }

        static Info CreateInfoForApiVersion(ApiVersionDescription description)
        {
            var info = new Info
            {
                Title = $"Products Catalog API {description.ApiVersion}",
                Version = description.ApiVersion.ToString(),
                Description = "API for managing products"
            };

            if (description.IsDeprecated)
            {
                info.Description += " This API version has been deprecated.";
            }

            return info;
        }
    }
}