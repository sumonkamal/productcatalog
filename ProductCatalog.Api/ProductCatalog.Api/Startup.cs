﻿using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using ProductCatalog.Api.Extensions;
using ProductCatalog.Api.Middlewares;
using ProductCatalog.Domain.Infrastructure;

namespace ProductCatalog.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IStartupConfigurationService externalStartupConfiguration)
        {
            Configuration = configuration;
            this._externalStartupConfigurationService = externalStartupConfiguration;
        }

        public IConfiguration Configuration { get; }
        private readonly IStartupConfigurationService _externalStartupConfigurationService;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMediatR(typeof(Domain.Commands.CreateProductCommand).Assembly);
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddApiVersioningWithSwagger();
            _externalStartupConfigurationService.ConfigureService(services, null);
            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
            IApiVersionDescriptionProvider provider, ILoggerFactory loggerFactory)
        {
            app.UseHttpContext();
            this._externalStartupConfigurationService.Configure(app, env, loggerFactory);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.ConfigureSwagger(provider);
            app.UseStaticFiles();
            app.UseMiddleware(typeof(ErrorHandlingMiddleware));
            app.ConfigureCors();
            app.UseMvc();
        }


    }
}
