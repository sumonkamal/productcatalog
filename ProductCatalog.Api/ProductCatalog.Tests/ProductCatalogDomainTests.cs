﻿using System.Threading;
using System.Threading.Tasks;
using Moq;
using ProductCatalog.Domain.Commands;
using ProductCatalog.Repository;
using ProductCatalog.Repository.Models;
using Xunit;

namespace ProductCatalog.UnitTests
{
    public class ProductCatalogDomainTests
    {
        [Fact]
        public async Task TestCreateProductCommand_ConstraintCheck_ShouldReturnErrorResponseWhenPriceIsZeroAndCodeAlreadyExists()
        {
            var repository = new Mock<IRepository>();
            repository.Setup(x => x.FindOne(It.IsAny<FindProductByCodeSpec>())).Returns(
                new Product() {Id = 1,Code = "ABC", Name = "Mobile Phone", Price = 10000 }
             );
         
            var handler = new CreateProductCommandHandler(repository.Object,new Mock<IFileService>().Object);
            var result = await handler.Handle(new CreateProductCommand("ABC", "Test", 0,null), new CancellationToken());
            Assert.True(result.HasError);
            Assert.Equal(2,result.Errors.Count);
            Assert.Equal(BusinessErrors.ProductWithSameCodeExists,result.Errors["Code"]);
            Assert.Equal(BusinessErrors.ProductPriceShouldBeGreaterThanZero, result.Errors["Price"]);
        }


    }
}
