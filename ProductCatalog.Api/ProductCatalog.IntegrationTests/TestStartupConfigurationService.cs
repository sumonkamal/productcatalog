﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ProductCatalog.Api;
using ProductCatalog.Repository;
using ProductCatalog.Repository.Models;

namespace ProductCatalog.IntegrationTests
{
    public class TestStartupConfigurationService : StartupConfigurationService, IStartupConfigurationService

    {
        public override void ConfigureEnvironment(IHostingEnvironment env)
        {
            env.EnvironmentName = "Test";
        }

        public override void ConfigureService(IServiceCollection services, IConfigurationRoot configuration)
        {
            base.ConfigureService(services, configuration);
            ConfigureStore(services);
        }

        protected virtual void ConfigureStore(IServiceCollection services)
        {
            services.AddScoped<IDatabase<ProductCatalogContext>, InMemoryDatabase<ProductCatalogContext>>();
        }

        public override void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            SetupStore(app);
        }

        protected virtual void SetupStore(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var sp = serviceScope.ServiceProvider;
                var repo = sp.GetRequiredService<IRepository>();
                repo.Create(new Product
                {
                    Code = "ABC",
                    Name = "Mobile",
                    Price = 120
                });

                repo.Create(new Product
                {
                    Code = "XYZ",
                    Name = "Notebook",
                    Price = 100
                });
                repo.Save();
                //TODO: Update Database and other stuff here
            }
        }


    }
}