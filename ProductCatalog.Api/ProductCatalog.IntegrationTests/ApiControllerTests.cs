using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProductCatalog.Api;
using ProductCatalog.Api.ViewModel;
using ProductCatalog.Domain.Queries;
using Xunit;

namespace ProductCatalog.IntegrationTests
{
    public static class ApiPaths
    {
        public const string Products = "/api/products";
    }
    public class ApiControllerTests : IClassFixture<TestFixture<Startup>>
    {
        private readonly HttpClient _client;
        public ApiControllerTests(TestFixture<Startup> fixture)
        {
            _client = fixture.Client;
        }

        [Fact]
        public async Task TestProductSearch_WithNameSpecified()
        {
            var httpResponse = await _client.GetAsync(ApiPaths.Products + "?Name=Notebook");
            // Must be successful.
            httpResponse.EnsureSuccessStatusCode();

            // Deserialize and examine results.
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var products = DeserializeObject<IEnumerable<ProductSearchResult>>(stringResponse);
            // There should be already created product with this name during seed
            Assert.Single(products);
        }

        [Fact]
        public async Task TestProductSearch_WithPriceSpecified()
        {
            var httpResponse = await _client.GetAsync(ApiPaths.Products + "?Price=100");

            // Must be successful.
            httpResponse.EnsureSuccessStatusCode();

            // Deserialize and examine results.
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var products = DeserializeObject<IEnumerable<ProductSearchResult>>(stringResponse);
            Assert.Single(products);
        }

        [Fact]
        public async Task Test_CreateProduct()
        {
            var vm = new ProductViewModel()
            {
                Name = "Suit",
                Code = "ABCD",
                Price = 12000,
                Picture = null
            };
            var dict = new Dictionary<string, string>
            {
                {nameof(vm.Name), vm.Name}, {nameof(vm.Price), vm.Price.ToString(CultureInfo.InvariantCulture)}, {nameof(vm.Code), vm.Code}
            };

            var httpResponse = await _client.PostAsync(ApiPaths.Products,
                new FormUrlEncodedContent(dict));
            httpResponse.EnsureSuccessStatusCode();

        }

        [Fact]
        public async Task Test_ProductCreateAndThen_UpdateThatProduct()
        {
            var vm = new ProductViewModel()
            {
                Name = "Suit",
                Code = "KLM",
                Price = 12000,
                Picture = null
            };
            var dict = new Dictionary<string, string>
            {
                {nameof(vm.Name), vm.Name}, {nameof(vm.Price), vm.Price.ToString(CultureInfo.InvariantCulture)}, {nameof(vm.Code), vm.Code}
            };

            var httpResponse = await _client.PostAsync(ApiPaths.Products,
                new FormUrlEncodedContent(dict));
            httpResponse.EnsureSuccessStatusCode();

           

            var getHttpResponse = await _client.GetAsync(ApiPaths.Products + "?Code=KLM");
            // Deserialize and get that product
            var stringResponse = await getHttpResponse.Content.ReadAsStringAsync();
            var product = DeserializeObject<IEnumerable<ProductSearchResult>>(stringResponse).First();
            var updateVm = new ProductViewModel()
            {
                Name = "Jacket",
                Code = "KLM",
                Price = 800,
                Picture = null
            };
            var updateDict = new Dictionary<string, string>
            {
                {nameof(updateVm.Name), updateVm.Name}, {nameof(updateVm.Price), updateVm.Price.ToString(CultureInfo.InvariantCulture)}, {nameof(updateVm.Code), updateVm.Code}
            };
            var updateResponse = await _client.PutAsync(ApiPaths.Products+"/"+product.Id+"/",
                new FormUrlEncodedContent(updateDict));
            updateResponse.EnsureSuccessStatusCode();
        }

        private T DeserializeObject<T>(string input)
        {
            return JsonConvert.DeserializeObject<T>(input);
        }
    }
}
