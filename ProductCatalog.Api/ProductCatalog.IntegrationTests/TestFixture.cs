using System;
using System.Net.Http;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using ProductCatalog.Api;

namespace ProductCatalog.IntegrationTests
{
    public class TestFixture<TStartup> : IDisposable where TStartup : class
    {
        private readonly TestServer _server;

        public TestFixture()
        {
            var builder = WebHost.CreateDefaultBuilder();
            builder.ConfigureServices(s => s.AddScoped<IStartupConfigurationService, TestStartupConfigurationService>());
            builder.UseStartup<Startup>();
            _server = new TestServer(builder);
            Client = _server.CreateClient();
        }

        public HttpClient Client { get; set; }

        public void Dispose()
        {
            Client.Dispose();
            _server.Dispose();
        }
    }
}