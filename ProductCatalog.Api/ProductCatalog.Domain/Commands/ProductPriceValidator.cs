﻿using ProductCatalog.Repository.Models;

namespace ProductCatalog.Domain.Commands
{
    public class ProductPriceValidator : BaseValidator, IValidator<Product>
    {
        public ValidationResult Validate(Product product)
        {
            if (product.Price <= 0)
            {
                Result.IsValid = false;
                Result.FieldName = nameof(product.Price);
                Result.ErrorMessage = BusinessErrors.ProductPriceShouldBeGreaterThanZero;
            }
            return Result;
        }
    }
}