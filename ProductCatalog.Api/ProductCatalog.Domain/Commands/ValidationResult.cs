﻿namespace ProductCatalog.Domain.Commands
{
    public class ValidationResult
    {
        public bool IsValid { get; set; }

        public string FieldName { get; set; }
        public string ErrorMessage { get; set; }
    }
}