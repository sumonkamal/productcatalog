﻿using System;
using System.Linq.Expressions;
using ProductCatalog.Repository.Models;
using ProductCatalog.Repository.Specification;

namespace ProductCatalog.Domain.Commands
{
    public class FindProductByIdSpec : SpecificationBase<Product>
    {
        readonly int _id;

        public FindProductByIdSpec(int id)
        {
            this._id = id;
        }

        public override Expression<Func<Product, bool>> SpecExpression
        {
            get
            {
                return product => product.Id == _id;
            }
        }
    }
}