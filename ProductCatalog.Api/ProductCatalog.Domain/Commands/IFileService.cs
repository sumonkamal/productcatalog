﻿using Microsoft.AspNetCore.Http;

namespace ProductCatalog.Domain.Commands
{
    public interface IFileService
    {
        string SaveFile(IFormFile file,string contentFolderName);
    }
}