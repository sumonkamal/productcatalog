﻿using MediatR;

namespace ProductCatalog.Domain.Commands
{
    public class DeleteProductCommand: IRequest<bool>
    {
        public DeleteProductCommand(int id)
        {
            ProductId = id;
        }
        public int ProductId { get; private set; }
    }
}
