﻿using System.Collections.Generic;

namespace ProductCatalog.Domain.Commands
{
    public class Response
    {
        public Response()
        {
            Errors = new Dictionary<string, string>();
        }
        public bool HasError { get; set; }

        public Dictionary<string,string> Errors { get; set; }

    }
}