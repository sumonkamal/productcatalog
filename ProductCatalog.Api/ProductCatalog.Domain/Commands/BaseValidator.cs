﻿namespace ProductCatalog.Domain.Commands
{
    public class BaseValidator
    {
        protected ValidationResult Result = new ValidationResult()
        {
            IsValid = true
        };

    }
}