﻿namespace ProductCatalog.Domain.Commands
{
    public static class BusinessErrors
    {
        public const string ProductPriceShouldBeGreaterThanZero = "Product price should be greater than zero";
        public const string ProductWithSameCodeExists = "Product with same code already exists";
    }
}