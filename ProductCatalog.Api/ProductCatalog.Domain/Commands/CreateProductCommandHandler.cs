﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ProductCatalog.Domain.Utils;
using ProductCatalog.Repository;
using ProductCatalog.Repository.Models;

namespace ProductCatalog.Domain.Commands
{
    public class CreateProductCommandHandler : ProductValidatorBase, IRequestHandler<CreateProductCommand, Response>
    {
        private readonly IRepository _repository;
        private readonly IFileService _fileService;
        public CreateProductCommandHandler(IRepository repository, IFileService fileService) : base(repository)
        {
            _repository = repository;
            _fileService = fileService;
        }
        public async Task<Response> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            string productPicture = null;
            if (request.Picture != null)
            {
                productPicture = _fileService.SaveFile(request.Picture, SystemConstants.ContentFolder);
            }
            var response = new Response();
            var product = new Product
            {
                Code = request.Code,
                Name = request.Name,
                Photo = productPicture,
                Price = request.Price
            };
            SetResponseFromErrors(product, response);
            if (response.HasError)
            {
                return response;
            }
            _repository.Create(product);
            await _repository.SaveAsync();
            return response;
        }
    }

}
