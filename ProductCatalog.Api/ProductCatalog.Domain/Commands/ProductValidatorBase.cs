﻿using System.Collections.Generic;
using System.Linq;
using ProductCatalog.Repository;
using ProductCatalog.Repository.Models;

namespace ProductCatalog.Domain.Commands
{
    public class ProductValidatorBase
    {
        private readonly List<IValidator<Product>> _validators = new List<IValidator<Product>>();

        public ProductValidatorBase(IReadOnlyRepository repository)
        {
            _validators.AddRange(new List<IValidator<Product>>
            {
                new ProductPriceValidator(),
                new UniqueProductCodeValidator(repository)
            });
        }
        public void AddValidators(IList<IValidator<Product>> validators)
        {
            _validators.AddRange(validators);
        }

        public virtual IEnumerable<ValidationResult> Validate(Product product)
        {
            return _validators.Select(validator => validator.Validate(product));
        }

        public void SetResponseFromErrors(Product product, Response response)
        {
            var result = Validate(product).ToList();
            if (result.Any(x => x.IsValid == false))
            {
                response.HasError = true;
                response.Errors = result.Where(x => !x.IsValid).ToDictionary(x => x.FieldName, x => x.ErrorMessage);
            }
        }


    }
}