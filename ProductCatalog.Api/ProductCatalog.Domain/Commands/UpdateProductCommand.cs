﻿using MediatR;
using Microsoft.AspNetCore.Http;

namespace ProductCatalog.Domain.Commands
{
    public class UpdateProductCommand : CreateProductCommand, IRequest<Response>
    {
        public UpdateProductCommand(int productId,string code, string name, IFormFile photo, decimal price) : base(code,name,price,photo)
        {
            ProductId = productId;
        }

        public int ProductId { get; set; }
    }
}
