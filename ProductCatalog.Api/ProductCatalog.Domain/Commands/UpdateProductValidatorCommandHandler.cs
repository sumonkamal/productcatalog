﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ProductCatalog.Domain.Utils;
using ProductCatalog.Repository;
using ProductCatalog.Repository.Models;

namespace ProductCatalog.Domain.Commands
{

    public class UpdateProductValidatorCommandHandler : ProductValidatorBase, IRequestHandler<UpdateProductCommand, Response>
    {

        private readonly IRepository _repository;
        private readonly IFileService _fileService;
        public UpdateProductValidatorCommandHandler(IRepository repository, IFileService fileService): base(repository)
        {
            _repository = repository;
            _fileService = fileService;
        }
        public async Task<Response> Handle(UpdateProductCommand request, CancellationToken cancellationToken)
        {
            var response = new Response();
            var productToUpdate = await _repository.GetByIdAsync<Product>(request.ProductId);
            if (productToUpdate == null) return response;

            if (request.Picture != null)
            {
                productToUpdate.Photo = _fileService.SaveFile(request.Picture, SystemConstants.ContentFolder);
            }
            productToUpdate.Code = request.Code;
            productToUpdate.Name = request.Name;
            productToUpdate.Price = request.Price;
            SetResponseFromErrors(productToUpdate, response);
            if (response.HasError)
            {
                return response;
            }
            _repository.Update(productToUpdate);
            await _repository.SaveAsync();
            return response;

        }
    }
}