﻿using System;
using System.Linq.Expressions;
using ProductCatalog.Repository.Models;
using ProductCatalog.Repository.Specification;

namespace ProductCatalog.Domain.Commands
{
    public class FindProductByCodeSpec : SpecificationBase<Product>
    {
        private readonly string _code;

        public FindProductByCodeSpec(string code)
        {
            this._code = code;
        }

        public override Expression<Func<Product, bool>> SpecExpression
        {
            get { return product => product.Code == _code; }

        }
    }
}