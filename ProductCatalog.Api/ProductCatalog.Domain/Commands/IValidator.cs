﻿using ProductCatalog.Repository.Models;

namespace ProductCatalog.Domain.Commands
{
    public interface IValidator<in TEntity> where TEntity : IEntity
    {
        ValidationResult Validate(TEntity entity);
    }
}