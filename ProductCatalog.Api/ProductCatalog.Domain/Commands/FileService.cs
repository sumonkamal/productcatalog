﻿using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace ProductCatalog.Domain.Commands
{
    public class FileService : IFileService
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ILogger<FileService> _logger;
        public FileService(IHostingEnvironment hostingEnvironment, ILogger<FileService> logger)
        {
            _hostingEnvironment = hostingEnvironment;
            this._logger = logger;
        }
        public string SaveFile(IFormFile file,string contentFolderName)
        {
            try
            {
                string fileName = "";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, contentFolderName);
                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }

                if (file.Length > 0)
                {
                    string extension = Path.GetExtension(file.FileName);
                    fileName = Guid.NewGuid() + extension;
                    string fullPath = Path.Combine(newPath, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }

                return fileName;
            }
            catch(Exception ex)
            {
                _logger.LogError($"Error in saving file. Error details: {ex.Message} {ex} {ex.InnerException}");
            }
            return String.Empty;
        }
    }
}