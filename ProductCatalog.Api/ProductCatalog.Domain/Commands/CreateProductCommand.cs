﻿using System.ComponentModel.DataAnnotations;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace ProductCatalog.Domain.Commands
{
    public class CreateProductCommand: IRequest<Response>
    {
        public CreateProductCommand(string code,string name,decimal price,IFormFile picture)
        {
            Code = code;
            Name = name;
            Picture = picture;
            Price = price;
        }

        [Required]
        public string Code { get; }
        [Required]
        public string Name { get; }
       // public string Photo { get; }

        public IFormFile Picture { get; set; }

        [Required]
        public decimal Price { get; }
    }
}
