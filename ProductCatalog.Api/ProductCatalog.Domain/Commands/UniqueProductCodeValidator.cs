﻿using ProductCatalog.Repository;
using ProductCatalog.Repository.Models;

namespace ProductCatalog.Domain.Commands
{
    public class UniqueProductCodeValidator : BaseValidator, IValidator<Product>
    {
        private readonly IReadOnlyRepository _repository;
        public UniqueProductCodeValidator(IReadOnlyRepository repository)
        {
            _repository = repository;
        }
        public ValidationResult Validate(Product product)
        {
            var existingProduct = _repository.FindOne(new FindProductByCodeSpec(product.Code));
            if (existingProduct != null && existingProduct.Id!= product.Id)
            {
                Result.IsValid = false;
                Result.FieldName = nameof(product.Code);
                Result.ErrorMessage = BusinessErrors.ProductWithSameCodeExists;
            }

            return Result;
        }
    }
}