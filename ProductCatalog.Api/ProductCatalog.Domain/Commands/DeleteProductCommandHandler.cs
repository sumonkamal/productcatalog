﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ProductCatalog.Repository;

namespace ProductCatalog.Domain.Commands
{
    public class DeleteProductCommandHandler : IRequestHandler<DeleteProductCommand, bool>
    {
        private readonly IReadOnlyRepository _readRepository;
        private readonly IRepository _repository;

        public DeleteProductCommandHandler(IRepository repository,
            IReadOnlyRepository readRepository)
        {
            _repository = repository;
            _readRepository = readRepository;
        }
        public async Task<bool> Handle(DeleteProductCommand request, CancellationToken cancellationToken)
        {
            var productToDelete =  _readRepository.FindOne(new FindProductByIdSpec(request.ProductId));
            if (productToDelete == null) return false;
            _repository.Delete(productToDelete);
            await _repository.SaveAsync();
            return true;
        }
    }
}