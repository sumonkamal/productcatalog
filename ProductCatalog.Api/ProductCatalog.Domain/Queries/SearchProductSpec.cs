﻿using System;
using System.Linq.Expressions;
using ProductCatalog.Repository.Models;
using ProductCatalog.Repository.Specification;

namespace ProductCatalog.Domain.Queries
{
    public class SearchProductSpec : SpecificationBase<Product>
    {
        private readonly string _name;
        private readonly string _code;
        private readonly decimal _price;
        public SearchProductSpec(string name,string code,decimal price)
        {
            this._name = name;
            this._code = code;
            this._price = price;

        }

        public override Expression<Func<Product, bool>> SpecExpression
        {
            get
            {
                return product => (string.IsNullOrEmpty(_name) || product.Name.ToLower().Contains(_name.ToLower()))
                                  && (string.IsNullOrEmpty(_code) || product.Code.ToLower().Contains(_code.ToLower()))
                                      && (_price == 0 || product.Price == _price);
            }
        }
    }
}