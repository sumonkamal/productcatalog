﻿using MediatR;

namespace ProductCatalog.Domain.Queries
{
    public class GetSingleProductQuery : IRequest<ProductSearchResult>
    {
        public int Id { get; private set; }

        public GetSingleProductQuery(int id)
        {
            this.Id = id;
        }
    }
}