﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ProductCatalog.Domain.Infrastructure;
using ProductCatalog.Domain.Utils;
using ProductCatalog.Repository;

namespace ProductCatalog.Domain.Queries
{
    public class SearchProductQueryHandler : IRequestHandler<SearchProductQuery, List<ProductSearchResult>>
    {
        private readonly IReadOnlyRepository _readOnlyRepository;
        public SearchProductQueryHandler(IReadOnlyRepository readOnlyRepository)
        {
            _readOnlyRepository = readOnlyRepository;
        }
        public Task<List<ProductSearchResult>> Handle(SearchProductQuery request, CancellationToken cancellationToken)
        {
            var products = _readOnlyRepository.Find(new SearchProductSpec(request.Name, request.Code, request.Price)).Select(x => new ProductSearchResult
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                Price = x.Price,
                Photo = !string.IsNullOrEmpty(x.Photo)? $"{MyHttpContext.AppBaseUrl}/{SystemConstants.ContentFolder}/{x.Photo}":x.Photo,
                LastUpdated = x.LastUpdated.ToString("dd-MM-yyyy HH:mm:ss")
            }).ToList();
            return Task.FromResult(products);
        }
    }
}