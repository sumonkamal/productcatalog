﻿using System.Collections.Generic;
using MediatR;

namespace ProductCatalog.Domain.Queries
{
    public class SearchProductQuery: IRequest<List<ProductSearchResult>>
    {
        public string Name { get; }
        public string Code { get; }
        public decimal Price { get; }
        public SearchProductQuery(string code,decimal price,string name)
        {
            Name = name;
            Code = code;
            Price = price;
        }
    }
}
