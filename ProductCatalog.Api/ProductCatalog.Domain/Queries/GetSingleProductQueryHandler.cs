﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ProductCatalog.Domain.Commands;
using ProductCatalog.Domain.Infrastructure;
using ProductCatalog.Domain.Utils;
using ProductCatalog.Repository;

namespace ProductCatalog.Domain.Queries
{
    public class GetSingleProductQueryHandler : IRequestHandler<GetSingleProductQuery, ProductSearchResult>
    {
        private readonly IReadOnlyRepository _readOnlyRepository;

        public GetSingleProductQueryHandler(IReadOnlyRepository readOnlyRepository)
        {
            _readOnlyRepository = readOnlyRepository;
        }
        public Task<ProductSearchResult> Handle(GetSingleProductQuery request, CancellationToken cancellationToken)
        {
            var product = _readOnlyRepository.FindOne(new FindProductByIdSpec(request.Id));
            var result = new ProductSearchResult
            {
                Code = product.Code,
                Name = product.Name,
                Price = product.Price,
                Id = product.Id,
                Photo = !string.IsNullOrEmpty(product.Photo)
                    ? $"{MyHttpContext.AppBaseUrl}/{SystemConstants.ContentFolder}/{product.Photo}"
                    : product.Photo,
                LastUpdated = product.LastUpdated.ToString("dd-MM-yyyy HH:mm:ss")
            };
            return Task.FromResult(result);
        }
    }
}