# ProductCatalog

Solution for managing product catalog

Product Catalog Solution:

Consists of 2 Applications as below:
 1. Backend API: ProductCatalog.Api
 2. Frond End UI: ProductCatalog.UI

- **ProductCatalog.Api**: The backend API is developed with Asp.Net Core Web API , Entity Framework and Microsoft SQL Server. It uses code first approach which will create the database for the first time.
Required project platform dependency for Back end API:
•	.Net Core 2.1
•	MS SQL Server

There is a default connection string in appsettings.json in the API root directory. Currently, in the connectionstring it is using the local server by using ‘.’ Which should be replaced with appropriate server name if required.
Technology/Framework Used:
-  	ASP.NET Core Web API
-  	Entity Framework Core with Code first approach
-  	MSSQL
-  	Swagger
-  	MediatR 
-  	Moq
-  	XUnit

There is a Unit Test and Integration projects for the Back End Part.

- **ProductCatalog.UI**: The front end is developed in Angular 7 with bootstrap 4.

Required dependencies:
- •	Node JS: Node.js version 8.x or 10.x.
- •	Install Angular CLI: npm install -g @angular/cli
Please not that : since the front end is calling the back-end api therefore Front end has to recognize back-end API Server URL which is specified in the **ProductCatalog.UI -> src-> environments- > environment.ts** file as a property named **apiBaseUrl: “http://localhost:50595”**  . Please change it if required (if the back end api url port is changed).

How to run the Applications:
- •	Run the back end api (Change the Connection string in appsettings.json if required)
- •	Go to the UI project directory and open a command prompt and run npm update
- •	Run -> ng serve . This might take a few minutes 
- •	By default the UI app should be listening to http://localhost:4200 if no port is specified during ng serve


If you face any problem running the application please contact me at-> **sumon154@gmail.com**







 

 

